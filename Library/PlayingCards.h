#include<iostream>
using namespace std;
struct Card
{
	int suit, rang;
	Card() :suit(-1), rang(-1) {};
	Card::Card(int s, int r)
	{
		if (s < 0 || s>3)
			throw "Illegal suit";
		if (r < 0 || r>12)
			throw "Illegal rang";
		suit = s;
		rang = r;
	}
};

class PlayingCards
{
public:
	PlayingCards() ;
	PlayingCards(int);
	PlayingCards(const PlayingCards &obj);
	PlayingCards(PlayingCards &&obj);
	PlayingCards(const Card&);
	~PlayingCards();
	friend istream&operator >>(istream&, PlayingCards&);
	friend ostream&operator <<(ostream&, const PlayingCards&);
	PlayingCards & operator ++();
	PlayingCards & operator =(const PlayingCards &obj);
	PlayingCards & operator =(PlayingCards &&obj);
	int getn() const;
	Card getFirstCard()const;
	int operator ()(int) const;
	int operator [] (int) const;
	PlayingCards& regulatPack();
	PlayingCards allotSub(int mas) const;


private:
	static const int SZ = 52;
	int n;
	Card *pack=nullptr;
	int findTheSame(int rang, int suit);
};
