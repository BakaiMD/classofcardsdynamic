// Application.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include<iostream>
#include<time.h>
#include "..\Library\PlayingCards.h"

using std::cout;
using std::cin;
using std::endl;
using std::cerr;


int getNum(int &res, const char *msg)
{
	using namespace std;
	bool error = 0;
	const char *p = "";
	do {
		cout << p << "Please , enter the  " << msg << " ";
		cin >> res;
		if (!cin.good())
		{
			if (cin.fail())
			{
				cin.clear();
				cin.ignore();
				error = 1;
				p = "Error!! REPEAT....";
			}
			if (cin.bad())
				return -1;
		}
		else error = 0;
	} while (error);
	return 1;
}


int startMenu() { // -------------------------------------------------------- ����� ����
	cout << "\n=============== Start menu ==================" << endl;
	cout << "1 - Default constructor\t 3 - Constructor with RANDOMIZE" << endl;
	cout << "2 Constructor with one card\t 4 - Copy Constructor" << endl;
	cout << "5- Constructor by hand" << endl;
	cout << "6 - Add card to pack ++\t 8 - Get rang of card []" << endl;
	cout << "7 Get suit of card ()\t 9 - Regulate pack of card" << endl;
	cout << "10- Get cards with the same suit\t 11 - Output cards" << endl;
	cout << "\t\t 12 - EXIT" << endl<<endl;
	int mode;
	do {
		getNum(mode, "choise");
	} while (mode < 1 || mode >12);
	return mode;
}



int main(int argc, char *argv[])
{
	srand(time(NULL));
	PlayingCards obj(6);
	PlayingCards pack=obj;
	bool done = true;
	while (done){
		switch (startMenu())
		{
		case 1:
		{
			PlayingCards Pack;
			pack = Pack;
			break;
		}
		case 2:
		{
			const char *msg = "";
			Card card;
			do {
				cout << msg << endl;
				getNum(card.suit, "suit");
				msg = "SUIT IS IMPOSSIBLE!  PLEASE REPEAT...";
			} while (card.suit < 0 || card.suit>3);
			msg = "";
			do {
				cout << msg << endl;
				getNum(card.rang, "rang");
				msg = "RANG IS IMPOSSIBLE!  PLEASE REPEAT...";
			} while (card.suit < 0 || card.suit>13);
			PlayingCards Pack(card);
			pack = Pack;
			break;
		}
		case 3:
		{
			int size;
			getNum(size, "number of cards");
			try
			{
				PlayingCards Pack(size);
				pack = Pack;
			}
			catch (const char* msg)
			{
				cerr << msg << endl;
			}
			break;
		}
		case 4:
		{
			PlayingCards Copy(6);
			cout << "Source pack" << Copy << endl;
			PlayingCards Pack(Copy);
			pack = Pack;
			cout << pack;
			break;
		}
		case 5:
		{
			int error, r;
			const char *p = "";
			do {

				cout << p << "Please , enter the CARDS" << " ";
				cin >> pack;
				if (!cin.good())
				{
					if (cin.fail())
					{
						cin.clear();
						while (cin.get() != '\n');
						error = 1;
						p = "Error!! REPEAT....";
					}
					if (cin.bad())
						return -1;
				}
				else error = 0;
			} while (error);
			break;
		}
		case 6: 
		{
			try
			{
				++pack;
				cout << "The card was added succesfully" << endl;
			}
			catch (const char* msg)
			{
				cerr << msg << endl;
			}
			break;
		}
		case 7:
		{
			int num;
			getNum(num, "number of card");
			try
			{
				cout << "Required suit is " << pack(num) << endl;
			}
			catch (const char *msg)
			{
				cerr << msg << endl;
			}
			break;
		}
		case 8:
		{int num;
		getNum(num, "number of card");
		try
		{
			cout << "Required rang is " << pack[num] << endl;
		}
		catch (const char *msg)
		{
			cerr << msg << endl;
		}
		break;
		}

		case 9:
		{
			pack.regulatPack();
			cout << "The pack was regulated" << endl;
			break;
		}
		case 10:
		{const char *msg = "";
		int suit;
		cout << msg << endl;
		getNum(suit, "suit");
		try {
			PlayingCards sgroup(6);
			sgroup=pack.allotSub(suit);
			cout << "Required suit group is" <<sgroup << endl;
		}
		catch (const char*msg)
		{
			cerr << msg << endl;
		}
		break;
		}
		case 11:
		{
			if (pack.getn() == 0)
			{
				cout << "The pack is empty" << endl;
				break;
			}
			cout << "===========Pack=========" << endl;
			cout << pack;
			break;
		}
		case 12:
			done = false;
			break;
		default:
			break;
		}
}
return 0;
}