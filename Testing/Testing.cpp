// Testing.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\Library\PlayingCards.h"
#include "gtest\gtest.h"
#include <math.h>
#include <time.h>



TEST(CardTesting, ConstructorDefaultTest)
{
	Card t1;
	ASSERT_NO_THROW(t1);
	ASSERT_DOUBLE_EQ(-1, t1.suit);// ��������� ������� ����������� 
	ASSERT_DOUBLE_EQ(-1, t1.rang);
}

TEST(CardTesting, ConstructorWithSuitAndRangTest)
{
	for (int suit = 0; suit < 4; ++suit)
	for (int rang = 0; rang < 13; ++rang)
	{
		Card t1(suit, rang);
		ASSERT_NO_THROW(Card (suit, rang))<<"There is unplanned exception";
		ASSERT_DOUBLE_EQ(suit, t1.suit)<<"����� ����� �� ����� ����� ����� � ������";
		ASSERT_DOUBLE_EQ(rang, t1.rang) << "���� ����� �� ����� ����� ����� � ������";
	}
}
TEST(CardTesting, ConstructorWithWrongSuitAndRangTest)
{
	for (int i = 0; i < 100; ++i)
	{
		int suit = 4 + rand() % 10000;
		for (int rang = 0; rang < 14;++rang)
		ASSERT_THROW(Card(suit, rang), const char*)<<"����� ��������� ����������� �����";
		suit = -1 - rand() % 10000;
		for (int rang = 0; rang < 14; ++rang)
			ASSERT_THROW(Card(suit, rang), const char*) << "����� ��������� ����������� �����";
	}
}
TEST(CardTesting, ConstructorWithSuitAndWrongRangTest)
{
	for (int i = 0; i < 100; ++i)
	{
		int rang = 13 + rand() % 10000;
		for (int suit = 0; suit < 4; ++suit)
			ASSERT_THROW(Card(suit, rang), const char*) << "����� ��������� ����������� ����";
		rang = -1 - rand() % 10000;
		for (int suit = 0; suit < 4; ++suit)
			ASSERT_THROW(Card(suit, rang), const char*) << "����� ��������� ����������� ����";
	}
}
TEST(PlayingCardsConstructorsTesting, DefaultTest)
{
	PlayingCards t1;
	ASSERT_EQ(0, t1.getn())<<"������� ����������� �������� �����������";
}


TEST(PlayingCardsConstructorsTesting, InitWithNumOfCardsTest)
{
	for (int i = 1; i < 52; ++i)
	{
		PlayingCards t1(i);
		ASSERT_NO_THROW(PlayingCards(i)) << "���������� ���������� ���� �� ��������� ��������";
		ASSERT_EQ(i, t1.getn()) << "���������� ���� ������������������ �� �����";
	}
	ASSERT_THROW(PlayingCards(53), const char*)<<"�� ����������� ��������� ������� ������";
	ASSERT_THROW(PlayingCards(0), const char*) << "�� ����������� ��������� ������� �����";
	for (int i = 0; i < 1; ++i)
	{
		int r = 53 + rand() % 1000;
		ASSERT_THROW(PlayingCards t12(r), const char*) << "������ ������� ����������� ����� ���������� ����";
		int r1 = 0 - rand() % 1000;
		ASSERT_THROW(PlayingCards t11(r1), const char*) << "������ ������� ����������� ����� ���������� ����";
	}
}


TEST(PlayingCardsConstructorsTesting, InitWithOneCardTest)
{
	for (int suit = 0; suit < 4; ++suit)
		for (int rang = 0; rang < 13; ++rang)
		{
			Card t1(suit, rang);
			PlayingCards p(t1);
			ASSERT_NO_THROW(PlayingCards(t1))<<"���������� ��������������� �� �����";
			ASSERT_EQ(suit, p.getFirstCard().suit)<<"����� ����� ������������������ �� �����";
			ASSERT_EQ(rang, p.getFirstCard().rang) << " ���� ����� ����������������� �� �����";
		}
	for (int i = 0; i < 100; ++i)
	{
		Card card;
		card.suit = 4 + rand() % 100;
		card.rang = rand()%13;
		ASSERT_THROW(PlayingCards t2(card), const char*)<<"������ ������������������ �������� ������ �����";
	}
	for (int i = 0; i < 100; ++i)
	{
		Card card;
		card.suit = rand()%4;
		card.rang = 13 + rand() % 100;
		ASSERT_THROW(PlayingCards t2(card), const char*) << "������ ������������������ �������� ������ ����";
	}
 
}

TEST(PlayingCardsConstructorsTesting, CopyConstructorTest)
{
	PlayingCards t;
	PlayingCards t1(t);
	ASSERT_EQ(0, t1.getn());
	for (int i = 1; i < 52; ++i)
	{
		PlayingCards t(i);
		PlayingCards t1(t);
		ASSERT_EQ(i, t1.getn())<<"���������� ���� �� ����� ���� �����";
		for (int j = 0; j < i; ++j) 
		{
			ASSERT_EQ(t(j), t1(j));
			ASSERT_EQ(t[j], t1[j]);
		}
	}
}

TEST(PlayingCardsOperatorTesting, AssignOperatorTest)
{
	for (int i = 1; i < 52; ++i)
	{
		PlayingCards t1(i);
		PlayingCards t2(5);
		PlayingCards t3;
		t3 = t2;
		t2 = t1;
		ASSERT_EQ(5, t3.getn());
		ASSERT_EQ(i, t2.getn());
		for (int j = 0; j < i; ++j) {
			ASSERT_EQ(t1(j), t2(j));
			ASSERT_EQ(t1[j], t2[j]);
		}
	}
}
TEST(PlayingCardsOperatorTesting, AddOperatorTest)
{
	PlayingCards t3;
	++t3;
	ASSERT_EQ(1, t3.getn());
	PlayingCards t1(52);
	ASSERT_THROW(++t1, const char*) << "�� �������������� ������������ ������";
	for (int i = 1; i < 52; ++i)
	{
		PlayingCards t2(i);
		++t2;
		ASSERT_EQ(i + 1, t2.getn())<<"��� ���������� ���������� ���� �� ����������";
		for (int j = 0; j < 51-i; ++j)
			++t2;
		ASSERT_THROW(++t2, const char*)<<"�� �������������� ������������ ������ � �����";
	}
}


TEST(PlayingCardsOperatorTesting, SuitOperatorTest)
{
	int r;
	PlayingCards t;
	ASSERT_THROW(t(0), const char*)<<"�������� ������ ����� � ������ ������";
	for (int i = 1; i < 53; ++i)
	{
		PlayingCards t1(i);
		for (int j = 0; j < i; ++j) {
			ASSERT_NO_THROW(t1(j)) << "�� ��������� ��������� ����� � ������";
			ASSERT_LE(0, t1(j)) << "�������� �������� �������� �����";
			ASSERT_GE(3, t1(j)) << "�������� �������� �������� �����";
		}
		for (int j = 0; j < 53; ++j)
		{
			r = i + rand() % 52;
			ASSERT_THROW(t1(r), const char*) << "��������� ����������� ����� � ������";
			r = -1 - rand() % 52;
			ASSERT_THROW(t1(r), const char*) << "��������� ����������� ����� � ������";
		}
	}
	for (int i= 0; i < 4; ++i)
	{
		Card p(i, 1);
		PlayingCards t(p);
		ASSERT_NO_THROW(t(0))<<"�� ��������� �������. ����� � ������";
		ASSERT_EQ(i, t(0))<<"����� ����� ������ ,����. ��������� ���������� �����, �� ��������� � ������ ����-�� �����";
	}
}



TEST(PlayingCardsOperatorTesting, RangOperatorTest)
{
	int r;
	PlayingCards t;
	ASSERT_THROW(t[0], const char*) << "�������� ������ ����� � ������ ������";
	for (int i = 1; i < 53; ++i)
	{
		PlayingCards t1(i);
		for (int j = 0; j < i; ++j) {
			ASSERT_NO_THROW(t1[j]) << "�� ��������� ��������� ����� � ������";
			ASSERT_LE(0, t1[j]) << "�������� �������� �������� �����";
			ASSERT_GE(12, t1[j]) << "�������� �������� �������� ����";
		}
		for (int j = 0; j < 53; ++j)
		{
			r = i + rand() % 52;
			ASSERT_THROW(t1[r], const char*) << "��������� ����������� ����� � ������";
			r = -1 - rand() % 52;
			ASSERT_THROW(t1[r], const char*) << "��������� ����������� ����� � ������";
		}
	}
	for (int i = 0; i < 13; ++i)
	{
		Card p(1, i);
		PlayingCards t(p);
		ASSERT_NO_THROW(t[0]) << "�� ��������� �������. ����� � ������";
		ASSERT_EQ(i, t[0]) << "���� ����� ������ ,����. ��������� ���������� �����, �� ��������� � ������ ����-�� �����";
	}
}


TEST(PlayingCardsMethodTesting, AllotTest)
{
	for (int i = 0; i < 4; ++i)
	{
		PlayingCards t;
		ASSERT_THROW(t.allotSub(i),const char*)<<"���-�� ����� �����. �����";
		PlayingCards t1(i +1);
		int suit = 4 + rand() % 100;
		ASSERT_THROW(t1.allotSub(suit), const char*)<<"���� ����������� �����";
	}

	for (int i = 0; i < 4; ++i)
	{
		Card card(i, 0);
		PlayingCards t2(card);
		ASSERT_NO_THROW(t2.allotSub(i));
		ASSERT_EQ(1, t2.allotSub(i).getn())<<"�� ������� ������ � �������� �������. ������";
		for (int j = 0; j < 4; ++j)
		{
			if (j == i)
				continue;
			ASSERT_THROW(t2.allotSub(j), const char*)<<"����������� ��������� ������ ���� �� �����(����� �� ������������ �����)";
		}
	}

	PlayingCards t3(52);
	for (int i = 0; i < 4; ++i)
	{
		ASSERT_NO_THROW(t3.allotSub(i));
		ASSERT_NO_THROW(13, t3.allotSub(i).getn())<<"��������� �� ������� ��� ������ ���� ����� �����";
	}
}


int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	::testing::InitGoogleTest(&argc, argv);
	int i = RUN_ALL_TESTS();
	system("pause");
	return i;
}

